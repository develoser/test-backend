from rest_framework.serializers import ModelSerializer
from .models import InputReport, Record


class InputReportSerializer(ModelSerializer):
    class Meta:
        model = InputReport
        fields = ('id', 'artist', 'title', 'isrc', 'duration', 'record')


class RecordSerializer(ModelSerializer):
    class Meta:
        model = Record
        fields = ('id', 'artist', 'title', 'isrc', 'duration')
