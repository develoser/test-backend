from django.conf.urls import url
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register('meta', views.InputReportListView)
router.register('records', views.RecordsListView)

urlpatterns = [
    url('meta/search', views.MetaSearchView.as_view()),
    url('meta/set', views.SetRecordInInput.as_view())
]

urlpatterns += router.urls
