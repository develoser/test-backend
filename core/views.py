from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.response import Response

from django.db.models import Q

from .models import InputReport, Record
from .serializers import InputReportSerializer, RecordSerializer

# Create your views here.


class InputReportListView(ModelViewSet):
    queryset = InputReport.objects.filter(record=None)
    serializer_class = InputReportSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['artist', 'title', 'isrc']
    ordering_fields = ['artist']


class RecordsListView(ModelViewSet):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['artist', 'title', 'isrc']
    ordering_fields = ['artist']


class MetaSearchView(APIView):
    def post(self, request):
        input_record = request.data["input_record"]
        if input_record["isrc"]:
            records = Record.objects.filter(
                      Q(isrc__iexact=input_record["isrc"])
            )
        else:
            records = Record.objects.filter(
                Q(artist__icontains=input_record["artist"])
                | Q(title__icontains=input_record["title"])
            )
        serializer = RecordSerializer(records, many=True)
        return Response(serializer.data)


class SetRecordInInput(APIView):
    def post(self, request):
        record_id = request.data["record_id"]
        input_record_id = request.data["input_record_id"]
        input_report = InputReport.objects.get(id=input_record_id)
        record = Record.objects.get(id=record_id)
        input_report.record = record
        input_report.save()
        serializer = InputReportSerializer(input_report)
        return Response(serializer.data)
