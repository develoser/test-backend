from django.contrib import admin
from .models import InputReport, Record

# Register your models here.
admin.site.register(InputReport)
admin.site.register(Record)
