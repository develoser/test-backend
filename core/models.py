from django.db import models


class Record(models.Model):
    artist = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    isrc = models.CharField(max_length=150, null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return 'Records:{} - {} - {} - {} - {}'.format(
            self.id, self.artist, self.title, self.isrc, self.duration)


class InputReport(models.Model):
    artist = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    isrc = models.CharField(max_length=150, null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    record = models.ForeignKey(Record, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return 'Input Report:{} - {} - {} - {} {} - {}'.format(
            self.id, self.artist, self.title, self.isrc, self.duration, self.record)
