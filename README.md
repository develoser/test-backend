# frontend-test

## Project setup
```
Create virtual environement and activate
pip install -r requirements
```
See, How to setup virtiual environment [Configuration Reference](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/) 
or [Reference Video(Mac & Linux)](https://www.youtube.com/watch?v=Kg1Yvry_Ydk), [Reference Video(Windows)](https://www.youtube.com/watch?v=APOPm01BVrk&t)

## Get Initial Data
```
python manage.py migrate
python manage.py loaddata frontend_test/fixtures/input_report.json
python manage.py loaddata frontend_test/fixtures/records.json
```

## Create Superuser For Database
```
python manage.py createsuperuser
```

### Compiles and hot-reloads for development
```
python manage.py runserver
```
